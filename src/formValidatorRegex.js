﻿var vRegex = {
	'integer' : {//整数
		regex : "^(-?[1-9]\\d*|0)$",
		focus : "请输入整数",
		error : "必须为整数"
	},
	'+integer' : {//正整数
		regex : "^[1-9]\\d*$",
		focus : "请输入正整数",
		error : "必须为正整数"
	},
	'-integer' : {//整负数
		regex : "^-[1-9]\\d*$",
		focus : "请输入负整数",
		error : "必须为负整数"
	},
	'+integer0' : {//正整数和0
		regex : "^([1-9]\\d*|0)$",
		focus : "请输入正数或0",
		error : "必须为正数或0"
	},
	'-integer0' : {//负整数和0
		regex : "^(-[1-9]\\d*|0)$",
		focus : "请输入负数或0",
		error : "必须为负数或0"
	},
	'num' : {//数字
		regex : "^([+-]?)\\d*\\.?\\d+$",
		focus : "请输入数字",
		error : "必须为数字"
	},
	'+num' : {
		regex : "^([1-9]\\d*.\\d*|0.\\d*[1-9]\\d*)$",
		focus : "请输入正数",
		error : "必须为正数"
	},
	'-num' : {
		regex : "^-([1-9]\\d*.\\d*|0.\\d*[1-9]\\d*)$",
		focus : "请输入负数",
		error : "必须为负数"
	},
	'+num0' : {//数字
		regex : "^([1-9]\\d*.\\d*|0.\\d*[1-9]\\d*|0?.0+|0)$",
		focus : "请输入正数或0",
		error : "必须为正数或0"
	},
	'-num0' : {//数字
		regex : "^((-([1-9]\\d*.\\d*|0.\\d*[1-9]\\d*))|0?.0+|0$)",
		focus : "请输入负数或0",
		error : "必须为负数或0"
	},
	'email' : {//邮件
		regex : "^\\w+((-\\w+)|(\\.\\w+))*\\@[A-Za-z0-9]+((\\.|-)[A-Za-z0-9]+)*\\.[A-Za-z0-9]+$",
		focus : "请输入邮箱",
		error : "邮箱格式不正确."
	},
	'url' : {//链接
		regex : "^http[s]?:\\/\\/([\\w-]+\\.)+[\\w-]+([\\w-./?%&=]*)?$",
		focus : "请输入链接,如 http://...",
		error : "链接格式有误"
	},
	'chinese' : {//仅中文
		regex : "^[\\u4E00-\\u9FA5\\uF900-\\uFA2D]+$",
		focus : "请输入中文",
		error : "仅限中文"
	},
	'ascii' : {//仅中文
		regex : "^[\\x00-\\xFF]+$",
		focus : "请输入ACSII字符",
		error : "仅限ACSII字符"
	},
	'zipcode' : {//邮编
		regex : "^\\d{6}$",
		focus : "请输入邮编",
		error : "邮编错误"
	},
	'cellphone' : {
		regex : "^13[0-9]{9}|15[012356789][0-9]{8}|18[0256789][0-9]{8}|147[0-9]{8}$",
		focus : "请输入手机号码",
		error : "手机号码有误"
	},
	'ip4' : {
		regex : "^((2[0-4]\d|25[0-5]|[01]?\d\d?)\.){3}(2[0-4]\d|25[0-5]|[01]?\d\d?)$",
		focus : "请输入IP(IP4)地址",
		error : "IP(IP4)地址有误"
	},
	'notEmpty' : {
		regex : "^\\S+$",
		focus : "请输入",
		error : "不允许为空"
	},
	'picture' : {
		regex : "(.*)\\.(jpg|bmp|gif|ico|pcx|jpeg|tif|png|raw|tga)$",
		focus : "支持扩展名:(jpg|bmp|gif|ico|pcx|jpeg|tif|png|raw|tga)",
		error : "图片不支持"
	},
	'rar' : {
		regex : "(.*)\\.(rar|zip|7zip|tgz)$",
		focus : "压缩文件:(jpg|bmp|gif|ico|pcx|jpeg|tif|png|raw|tga)",
		error : "文件格式不支持"
	},
	'qq' : {
		regex : "^[1-9]*[1-9][0-9]*$",
		focus : "请输入QQ号码",
		error : "QQ号码有误"
	},
	'tel' : {
		regex : "^(([0\\+]\\d{2,3}-)?(0\\d{2,3})-)?(\\d{7,8})(-(\\d{3,}))?$",
		focus : "请输入电话号码(支持国际区号,国内区号,分机号)",
		error : "电话号码有误"
	},
	'username' : {
		regex : "^\\w+$",
		focus : "请输入数字、26个英文字母或者下划线组成的字符串",
		error : "包含非法字符"
	},
	'letter' : {
		regex : "^[A-Za-z]+$",
		focus : "请输入字母",
		error : "包含非法字符"
	},
	'letter_u' : {
		regex : "^[A-Z]+$",
		focus : "请输入大写字母",
		error : "包含非法字符"
	},
	'letter_l' : {
		regex : "^[a-z]+$",
		focus : "请输入小写字母",
		error : "包含非法字符"
	},
	'idcard' : {
		focus : "请输入身份证号",
		fun : function(sId){
			var aCity={11:"北京",12:"天津",13:"河北",14:"山西",15:"内蒙古",21:"辽宁",22:"吉林",23:"黑龙江",31:"上海",32:"江苏",33:"浙江",34:"安徽",35:"福建",36:"江西",37:"山东",41:"河南",42:"湖北",43:"湖南",44:"广东",45:"广西",46:"海南",50:"重庆",51:"四川",52:"贵州",53:"云南",54:"西藏",61:"陕西",62:"甘肃",63:"青海",64:"宁夏",65:"新疆",71:"台湾",81:"香港",82:"澳门",91:"国外"};
			var iSum=0 ;
			if(!/^\d{17}(\d|x)$/i.test(sId)) {
				msg = "你输入的身份证长度或格式错误";
				return false; 
			}
			sId=sId.replace(/x$/i,"a"); 
			if(aCity[parseInt(sId.substr(0,2))]==null){
				msg = "你的身份证地区非法";
				return false; 
			}
			sBirthday=sId.substr(6,4)+"-"+Number(sId.substr(10,2))+"-"+Number(sId.substr(12,2)); 
			var d=new Date(sBirthday.replace(/-/g,"/")) ;
			if(sBirthday!=(d.getFullYear()+"-"+ (d.getMonth()+1) + "-" + d.getDate())){
				msg =  "身份证上的出生日期非法"; 
				return false; 
			}
			for(var i = 17;i>=0;i --) iSum += (Math.pow(2,i) % 11) * parseInt(sId.charAt(17 - i),11) ;
			if(iSum%11!=1) {
				msg =  "你输入的身份证号非法"; 
				return false; 
			}
			return true;//aCity[parseInt(sId.substr(0,2))]+","+sBirthday+","+(sId.substr(16,1)%2?"男":"女") 
		}
	},
	'date' : {
		regex : "^\\d{4}-\\d{1,2}-\\d{1,2}$",
		focus : "请输入日期,如 2012-12-12",
		error : "时间格式有误"
	},
	'time' : {
		regex : "^(\d{1,2})(:)?(\d{1,2})\2(\d{1,2})$",
		focus : "请输入时间, 11:02:08",
		error : "时间格式有误"
	},
	'datetime' : {
		regex : "^\\d{4}-\\d{1,2}-\\d{1,2} (\d{1,2})(:)?(\d{1,2})\2(\d{1,2})$",
		focus : "请输入时间, 2012-12-12 11:02:08",
		error : "时间格式有误",
		show : ""
	}
};
