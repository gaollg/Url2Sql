package test;

import javax.servlet.http.HttpSession;

import net.vsame.url2sql.action.BaseAction;
import net.vsame.url2sql.exception.Url2SqlErrorException;
import net.vsame.url2sql.helper.SqlHelper;

public class TestAction extends BaseAction{
	
	public void a(){
		System.out.println("method a.");
		context.put("a", "this is use java method a.");
		HttpSession session = context.getServletSession();
		session.setAttribute("aaa", "bbbb");
		System.out.println(context.getSessionVal("aaa", String.class));
		
		StudentBean bean = new StudentBean();
		bean.setAge(9);
		bean.setName("gaollg");
		session.setAttribute("stu", bean);
		
		System.out.println(context.getSessionVal("stu"));
		System.out.println(context.getSessionVal("stu", StudentBean.class));
		System.out.println(context.getSessionVal("stu.age", int.class));
		
		
	}
	public void b(){
		System.out.println("method b.");
		context.put("b", "this is use java method b.");
		
		Url2SqlErrorException.error(3000, "错错错");
		
		String queryAll = urlConfig.getVar("queryAll");
		context.put("queryAll", queryAll);
		context.put("queryAllResult", SqlHelper.query(queryAll));
		context.put("queryById", SqlHelper.query("querybyId", 1, 2, 3, 4, 5));
		
	}
	
	//======================================	
	public void querybyId(){
		String querybyId = urlConfig.getVar("querybyId");
		context.put("querybyIdResult", SqlHelper.query(querybyId));
	}
	public void add(){
		String sql = urlConfig.getVar("1");
		context.put("eneratedId", SqlHelper.executeGeneratedKey(sql));
	}
	
	public void updata(){
		String sql = urlConfig.getVar("1");
		context.put("result", SqlHelper.execute(sql));
	}
	
	public void delete(){
		String sql = urlConfig.getVar("1");
		context.put("result", SqlHelper.execute(sql));
	}
	
	public void queryAll(){
		String sql = urlConfig.getVar("1");
		context.put("querybyIdResult", SqlHelper.query(sql));
	}
	
	public void page(){
		String sql = urlConfig.getVar("sql");
		context.put("querybyIdResult", SqlHelper.page(sql));

	}

}
