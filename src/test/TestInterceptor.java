package test;

import java.util.List;

import javax.servlet.ServletContext;

import net.vsame.url2sql.helper.CacheHelper;
import net.vsame.url2sql.url.Interceptor;
import net.vsame.url2sql.url.impl.CacheInterceptor;
import net.vsame.url2sql.url.impl.UrlMapping;
import net.vsame.url2sql.url.impl.UrlMapping.Chain;

public class TestInterceptor implements Interceptor{

	@Override
	public void init(UrlMapping urlMapping, ServletContext servletContext) {
		System.out.println("==========================init==================");
		//SqlHelper.setJdbcUtils(new OracleJdbcUtils());
		
		CacheInterceptor.setCacheHelper(new CacheHelper() {
			
			@Override
			public void saveCache(String cacheKey, String cacheJson, Long time) {
				System.out.println("******* saveCache start ********");
				System.out.println(cacheKey);
				System.out.println(cacheJson);
				System.out.println(time);
				System.out.println("******* saveCache  end  ********");
			}
			
			@Override
			public String readCache(String cacheKey) {
				System.out.println("******* readCache start ********");
				System.out.println(cacheKey);
				System.out.println("******* readCache  end  ********");
				return null;
			}
			
			@Override
			public String getSessionId() {
				return null;
			}
			
			@Override
			public void delCacheBefore(List<String> cleanPaths) {
				System.out.println("******* delCacheBefore start ********");
				System.out.println(cleanPaths);
				System.out.println("******* delCacheBefore  end  ********");
			}
		});
	}

	@Override
	public void invoke(Chain chain) {
		System.out.println("-----------------------invoke start------------------");
		long begin = System.currentTimeMillis();
		try {
			chain.next();
		} finally {
			System.out.println((System.currentTimeMillis()-begin) + "ms");
			System.out.println("-----------------------invoke end------------------");
		}
		
	}

	@Override
	public void destroy() {
		
	}

	
}
