package net.vsame.url2sql.exception;

import net.vsame.url2sql.helper.Url2SqlContext;
import net.vsame.url2sql.helper.WebHelper;

public class Url2SqlErrorException extends RuntimeException{
	private static final long serialVersionUID = 1L;

	/**
	 * Url2sql 业务异常
	 * @param code
	 * @param msg
	 * @param clean true表示清除原有数据
	 */
	public Url2SqlErrorException(int code, String msg, boolean clean) {
		super(msg);
		Url2SqlContext c = WebHelper.getContext();
		c.putError(code, msg, clean);
	}
	
	/**
	 * Url2sql 业务异常
	 * @param code
	 * @param msg
	 */
	public Url2SqlErrorException(int code, String msg) {
		this(code, msg, true);
	}
	
	/**
	 * 抛出 Url2sql 业务异常
	 * @param code
	 * @param msg
	 * @param clean true表示清除原有数据
	 */
	public static void error(int code, String msg, boolean clean) {
		throw new Url2SqlErrorException(code, msg, clean);
	}
	
	
	/**
	 * 抛出 Url2sql 业务异常
	 * @param code
	 * @param msg
	 */
	public static void error(int code, String msg) {
		throw new Url2SqlErrorException(code, msg);
	}
}
