package net.vsame.url2sql.config;

import java.util.Iterator;
import java.util.ServiceLoader;

import net.vsame.url2sql.url.impl.UrlMapping;

public class ConfigFactory {
	
	public static void load(UrlMapping mapping){
		//TODO 需加载MATE-INF/
		ServiceLoader<InitConfig> sl = ServiceLoader.load(InitConfig.class);
		Iterator<InitConfig> it = sl.iterator();
		while (it.hasNext()) {
			InitConfig m = it.next();
			m.load(mapping);
		}
		//new XmlInitConfig().load(mapping);
	}

}
