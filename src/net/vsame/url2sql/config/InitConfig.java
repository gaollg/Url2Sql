package net.vsame.url2sql.config;

import net.vsame.url2sql.url.impl.UrlMapping;

public interface InitConfig {
	
	void load(UrlMapping mapping);

}
