package net.vsame.url2sql.render;

import java.io.Serializable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 渲染视图
 */
public abstract class Render implements Serializable {
	
	private static final long serialVersionUID = 8606198027209535420L;
	protected transient HttpServletRequest request;
	protected transient HttpServletResponse response;
	protected transient String encoding;
	
	
	public final Render setContext(HttpServletRequest request, HttpServletResponse response) {
		this.request = request;
		this.response = response;
		return this;
	}
	
	/**
	 * 渲染方法
	 */
	public abstract void render();

	public String getEncoding() {
		return encoding;
	}
	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}
}
