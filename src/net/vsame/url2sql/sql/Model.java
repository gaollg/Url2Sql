package net.vsame.url2sql.sql;

import java.util.HashMap;
import java.util.Map;

import net.vsame.url2sql.helper.TypeConvertHelper;

public class Model extends HashMap<String, Object>{

	private static final long serialVersionUID = 1L;
	
	public Model(){
	}
	
	public Model(Map<String, Object> map) {
		this.putAll(map);
	}

	public static Model get(){
		return new Model();
	}
	
	public Model add(String key, Object value){
		this.put(key, value);
		return this;
	}
	public <T> T getType(Class<T> clazz, String key){
		return TypeConvertHelper.parseType(clazz, get(key));
	}
	public <T> T getType(Class<T> clazz, String key, T defVal){
		return TypeConvertHelper.parseType(clazz, get(key), defVal);
	}
	
	public Short getShort(String key){
		return getType(Short.class, key);
	}
	public Short getShort(String key, Short defVal){
		return getType(Short.class, key, defVal);
	}
	public Integer getInt(String key){
		return getType(Integer.class, key);
	}
	public Integer getInt(String key, Integer defVal){
		return getType(Integer.class, key, defVal);
	}
	public Long getLong(String key){
		return getType(Long.class, key);
	}
	public Long getLong(String key, Long defVal){
		return getType(Long.class, key, defVal);
	}
	public Float getFloat(String key){
		return getType(Float.class, key);
	}
	public Float getFloat(String key, Float defVal){
		return getType(Float.class, key, defVal);
	}
	public Double getDouble(String key){
		return getType(Double.class, key);
	}
	public Double getDouble(String key, Double defVal){
		return getType(Double.class, key, defVal);
	}
	public String getString(String key){
		return getType(String.class, key);
	}
	public String getString(String key, String defVal){
		return getType(String.class, key, defVal);
	}

	public static void main(String[] args) {
		Model m = Model.get().add("a", "123.32").add("b", "3");
		System.out.println(m.getDouble("a"));
		System.out.println(m.getFloat("a"));
		System.out.println(m.getInt("b"));
		System.out.println(m.getLong("b"));
	}
	
}
