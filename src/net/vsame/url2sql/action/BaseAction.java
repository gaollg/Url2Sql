package net.vsame.url2sql.action;

import net.vsame.url2sql.helper.Url2SqlContext;
import net.vsame.url2sql.helper.WebHelper;
import net.vsame.url2sql.url.impl.UrlConfig;

/**
 * 基础父类
 * @author gaolei
 *
 */
public class BaseAction {
	
	protected Url2SqlContext context;
	protected UrlConfig urlConfig;

	public BaseAction(){
		context = WebHelper.getContext();
		urlConfig = context.getUrlConfig();
	}

}
