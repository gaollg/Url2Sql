package net.vsame.url2sql.utils.dataBase;

import java.util.ArrayList;
import java.util.List;

import net.vsame.url2sql.helper.Url2SqlContext;
import net.vsame.url2sql.helper.WebHelper;
import net.vsame.url2sql.sql.Model;
import net.vsame.url2sql.utils.JdbcUtils;
import net.vsame.url2sql.utils.PageView;

public class OracleJdbcUtils extends JdbcUtils {

	@Override
	public PageView page(int currentpage, int pagesize, String sql,
			Object... values) {
		
		PageView v = new PageView(pagesize, currentpage);
		long count = queryCount("select count(*) count from (" + sql + ") b", values);
		v.setTotal(count);
		
		if(v.calcFirstResult() < count){
			String temp = "SELECT * FROM (SELECT B.*, ROWNUM RownumVar FROM (" + sql + ") B WHERE ROWNUM <=" +
					(v.calcFirstResult() + v.getPagesize()) + ") WHERE RownumVar>"  + v.calcFirstResult();
			List<Model> datas = queryList(temp, values);
			v.setDatas(datas);
		}else{
			v.setDatas(new ArrayList<Model>());
		}
		return v;
	}
	
	public static void main(String[] args) throws Exception {
		WebHelper.init(null, null);
		Url2SqlContext c = WebHelper.getContext();
		c.getConn().setAutoCommit(true);
		
		JdbcUtils jdbcUtils = new OracleJdbcUtils();
		
		PageView page = jdbcUtils.page(1, 2, "select * from ac01 where aab001 = '10113004274'");
		System.out.println(page);
		
	}
	
}
