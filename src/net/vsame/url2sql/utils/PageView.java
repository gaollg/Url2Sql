package net.vsame.url2sql.utils;

import java.util.List;

import net.vsame.url2sql.sql.Model;

public class PageView {
	/** 分页数据 */
	private List<Model> datas;
	/** 总记录数 */
	private long total;
	/** 每页显示记录数 */
	private int pagesize = 12;
	/** 当前页 */
	private int currentpage = 1;
	/** 总页数 */
	private long totalpage = 1;
	
	/**
	 * 计算开始索引
	 * @return
	 */
	public int calcFirstResult(){
		return (currentpage-1) * pagesize;
	}
	public PageView(int pagesize, int currentpage) {
		this.pagesize = pagesize;
		this.currentpage = currentpage;
	}
	public List<Model> getDatas() {
		return datas;
	}
	public void setDatas(List<Model> datas) {
		this.datas = datas;
	}
	public long getTotalpage() {
		return totalpage;
	}
	public void setTotalpage(long totalpage) {
		this.totalpage = totalpage;
	}
	public int getPagesize() {
		return pagesize;
	}
	public void setPagesize(int pagesize) {
		this.pagesize = pagesize;
	}
	public int getCurrentpage() {
		return currentpage;
	}
	public void setCurrentpage(int currentpage) {
		this.currentpage = currentpage;
	}
	
	public long getTotal() {
		return total;
	}
	public void setTotal(long total) {
		this.total = total;
		setTotalpage(total%pagesize==0 ? total/pagesize : total/pagesize+1);
	}
	@Override
	public String toString() {
		return "PageView [datas=" + datas + ", total=" + total + ", pagesize="
				+ pagesize + ", currentpage=" + currentpage + ", totalpage="
				+ totalpage + "]";
	}
}
