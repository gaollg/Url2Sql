package net.vsame.url2sql.helper;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.vsame.url2sql.sql.Model;
import net.vsame.url2sql.utils.JdbcUtils;
import net.vsame.url2sql.utils.PageView;

/**
 * </p>Sql工具, ${}中占位符值按优先级排列如下：</p>
 * <ol>
 * <li>从传递MAP取, 当然也可以不传</li>
 * <li>从request.getParameterMap();取</li>
 * <li>从url中rest参数取，注意写法为${0}</li>
 * </ol>
 * <p> Sql Example: select * from student where id in(${ids})</p>
 * 
 * @author <a href="mailto:gaollg@sina.com">Gaollg</a>
 *
 */
public class SqlHelperInstance {
	private static final Pattern pattern = Pattern.compile("\\$\\{(.*?)\\}|\\?");
	
	//===============自定义标签====================
	/**
	 * 自定义标签使用时${~tag:param1:param2:param3}
	 */
	public static interface CustomTag {
		/**
		 * @return 返回自定义Tag名称，注意，Tag名称不区分大小写
		 */
		public String getTagName();
		/**
		 * @param params 自定义标签使用时${~tag:param1:param2:param3} 参数param值为[param1, param2, param3]
		 * @return 返回自定义标签处理结果
		 */
		public Object invoke(String[] params);
	}
	
	private static Map<String, CustomTag> customTagMap = new HashMap<String, SqlHelperInstance.CustomTag>();
	
	private JdbcUtils jdbcUtils = new JdbcUtils();
	public SqlHelperInstance() {
		this.jdbcUtils = new JdbcUtils();
	}
	public SqlHelperInstance(JdbcUtils jdbcUtils) {
		this.jdbcUtils = jdbcUtils;
	}


	public static void registCustomTag(CustomTag customTag){
		customTagMap.put(customTag.getTagName().toLowerCase(), customTag);
	}
	
	private static Object calcCustomTagValue(String key){
		String[] arrays = key.split(":");
		CustomTag ct = customTagMap.get(arrays[0].toLowerCase());
		if(ct == null){
			throw new RuntimeException("No CustomTag name ~" + key + "!!!");
		}
		String[] params = Arrays.copyOfRange(arrays, 1, arrays.length);
		return ct.invoke(params);
	}
	
	static {
		registCustomTag(new CustomTag() {
			@Override
			public Object invoke(String[] params) {
				return UUID.randomUUID().toString();
			}

			@Override
			public String getTagName() {
				return "uuid";
			}
		});
	}
	
	///////////////////////////////////////////////开始
	private static class Option{
		String sql;
		Object[] values;
		
		public Option(String sql, Object[] values) {
			this.sql = sql;
			this.values = values;
		}
	}
	
	public static Object[] toObjectArray(Object obj){
		if((obj==null) || !obj.getClass().isArray()){
			return new Object[]{obj};
		}
		int length = Array.getLength(obj);
		Object[] retVal = new Object[length];
		for (int i = 0; i < length; i++) {
			retVal[i] = Array.get(obj, i);
		}
		return retVal;
	}
	
	/**
	 * </p>Sql工具, ${}中占位符值按优先级排列如下：</p>
	 * <ol>
	 * <li>从传递MAP取, 当然也可以不传</li>
	 * <li>从request.getParameterMap();取</li>
	 * <li>从url中rest参数取，注意写法为${0}</li>
	 * </ol>
	 * <p> Sql Example: select * from student where id in(${ids})</p>
	 * 
	 * @param sql
	 * @param objects
	 * @return
	 */
	public static Option getCommOption(String sql, Object...objects){
		
		Url2SqlContext context = WebHelper.getContext();
		
		Map<String, String[]> map = context.getParams();
		//如果Sql长度小于10, 认为是变量名
		if(sql.startsWith("$")){
			sql = context.getUrlConfig().getVar(sql.substring(1));
		}
		else if (sql.length() <= 10) {
			sql = context.getUrlConfig().getVar(sql);
		}
		
		//开始匹配
		List<Object> values = new ArrayList<Object>();
		Matcher m = pattern.matcher(sql);
		StringBuffer sb = new StringBuffer();
		int index = 0;//? 占位符
		while(m.find()){
			Object[] value = null;
			if("?".equals(m.group(0))) {
				//? 占位符
				value = toObjectArray(objects[index]);
				index ++;
			}else {//${}表达式
				String key = m.group(1);
				if(key.toLowerCase().startsWith("session.")){
					//从Session中取
					Object temp = context.getSessionVal(key.substring(8));
					if(temp == null){
						throw new SqlHelper.ParamNotExistException("${" + key + "} in session must exist.");
					}
					value = toObjectArray(temp);
					
				} else if(key.startsWith("~")){
					Object o = calcCustomTagValue(key.substring(1, key.length()));
					value = toObjectArray(o);
				} else{
					if(!map.containsKey(key)){
						throw new SqlHelper.ParamNotExistException("${" + key + "} must exist.");
					}
					value = toObjectArray(map.get(key));
				}
			}
			
			//拼Sql
			StringBuffer temp = new StringBuffer();
			for(Object s: value){
				temp.append("?,");
				values.add(s);
			}
			temp.deleteCharAt(temp.length()-1);
			m.appendReplacement(sb, temp.toString());
		}
		m.appendTail(sb);
		
		return new Option(sb.toString(), values.toArray());
	}
	
	/**
	 * 查询多个
	 * @param sql
	 * @param objects
	 * @return
	 */
	public List<Model> query(String sql, Object...objects){
		Option o = getCommOption(sql, objects);
		return jdbcUtils.queryList(o.sql, o.values);
	}
	
	
	/**
	 * 分页
	 * @param currentpage 当前页
	 * @param pagesize 每页显示数
	 * @param sql 
	 * @return
	 */
	public PageView page(int currentpage, int pagesize, String sql, Object...objects){
		Option o = getCommOption(sql, objects);
		return jdbcUtils.page(currentpage, pagesize, o.sql, o.values);
	}
	
	/**
	 * 分页
	 * @param sql 
	 * @return
	 */
	public PageView page(String sql, Object...objects){
		Option o = getCommOption(sql, objects);
		Url2SqlContext c = WebHelper.getContext();
		Integer currentpage = c.getParamByType(Integer.class, "currentpage"); 
		if(currentpage == null){
			currentpage = 1;
		}
		Integer pagesize = c.getParamByType(Integer.class, "pagesize"); 
		if(pagesize == null){
			pagesize = 10;
		}
		return jdbcUtils.page(currentpage, pagesize, o.sql, o.values);
	}
	
	/**
	 * 查询一个
	 * @param sql
	 * @param objects
	 * @return
	 */
	public Model queryOne(String sql, Object...objects){
		Option o = getCommOption(sql, objects);
		return jdbcUtils.queryOne(o.sql, o.values);
	}
	
	/**
	 * 执行Sql，返回影响的条数
	 * @param sql
	 * @param objects
	 * @return
	 */
	public int execute(String sql, Object...objects){
		Option o = getCommOption(sql, objects);
		return jdbcUtils.execute(o.sql, o.values);
	}
	
	/**
	 * 执行Sql，返回自动生成的列，比如主健（GENERATED_KEY），时间
	 * @param sql
	 * @param objects
	 * @return
	 */
	public Long executeGeneratedKey(String sql, Object...objects){
		Option o = getCommOption(sql, objects);
		return jdbcUtils.executeGeneratedKey(o.sql, o.values);
	}
	
	public List<String> alltables(){
		return jdbcUtils.getAllTables();
	}
	
	
	public static void main(String[] args) {
		WebHelper.init(null, null);
		SqlHelperInstance instance = new SqlHelperInstance();
		
		List<Model> a = instance.query("select * from student where id not in(${~uuid:yyyy})");
		System.out.println(a);
		
		Url2SqlContext c = WebHelper.getContext();
		Map<String, String[]> m = new HashMap<String, String[]>();
		c.setParams(m);
		m.put("a.id", new String[]{"2"});
		m.put("a.name", new String[]{"gaollg"});
		List<Model> result = instance.query("select * from student where id=${a.id} and name like ?", "gaollg");
		System.out.println(result);
		
		m.clear();
		m.put("ids", new String[]{"1", "2", "3"});
		result = instance.query("select * from student where id in(${ids}) or id in(${~uuid})");
		System.out.println(result);
		
		System.out.println("-----------------");
		result = instance.query("select * from student where id in(?) or id in(${~uuid})", new String[]{"1", "2", "3"}, 0);
		System.out.println(result);
		
		m.clear();
		m.put("name", new String[]{"Java"});
		m.put("age", new String[]{"44"});
//		long id = executeGeneratedKey("INSERT INTO `student` (`name`, `age`) VALUES ('Huhu', '0')", "");
//		System.out.println(id);
//		int count = execute("INSERT INTO `student` (`name`, `age`) VALUES ('Huhu', '0')", map);
//		System.out.println("count:" + count);
		WebHelper.remove();
	}
	
	public static void main2(String[] args) {
		String a = "a?b?c?d?e?f?ghijklmn?";
		StringBuffer sb = new StringBuffer();
		String key = "?KEY&=";//把这些Url关键字全占了, 不信还真有这样的参数名称
		char[] array = a.toCharArray();
		for(int i=0; i<array.length; i++){
			char c = array[i];
			if(c == '?'){
				sb.append(key + i);
			}else {
				sb.append(c);
			}
		}
		System.out.println(sb);
	}
	
}
