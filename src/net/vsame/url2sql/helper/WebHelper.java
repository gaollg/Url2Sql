package net.vsame.url2sql.helper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 网页辅助类
 * 2012-7-1 上午9:29:46
 * @author <a href="mailto:gaollg@sina.com">Gaollg</a>
 *
 */
public class WebHelper {
	
	private static ThreadLocal<Url2SqlContext> context = new ThreadLocal<Url2SqlContext>();

	public static void init(HttpServletRequest request, HttpServletResponse response) {
		Url2SqlContext temp = new Url2SqlContext();
		temp.setRequest(request);
		temp.setResponse(response);
		WebHelper.context.set(temp);
	}
	
	public static Url2SqlContext getContext() {
		Url2SqlContext temp = context.get();
		if(temp == null){
			temp = new Url2SqlContext();
			WebHelper.context.set(temp);
		}
		return temp;
	}
	/**
	 * 销毁,在每次请求完成后,调用回收
	 */
	public static void remove(){
		Url2SqlContext temp = getContext();
		try {
			if(temp != null){
				temp.destroy();
			}
		} catch (Exception e) {
		}finally {
			context.remove();
		}
	}
	
}
