package net.vsame.url2sql.helper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.vsame.url2sql.sql.Model;
import net.vsame.url2sql.utils.JdbcUtils;
import net.vsame.url2sql.utils.PageView;

/**
 * </p>Sql工具, ${}中占位符值按优先级排列如下：</p>
 * <ol>
 * <li>从传递MAP取, 当然也可以不传</li>
 * <li>从request.getParameterMap();取</li>
 * <li>从url中rest参数取，注意写法为${0}</li>
 * </ol>
 * <p> Sql Example: select * from student where id in(${ids})</p>
 * 
 * @author <a href="mailto:gaollg@sina.com">Gaollg</a>
 *
 */
public class SqlHelper {
	
	@SuppressWarnings("serial")
	public static class ParamNotExistException extends RuntimeException {

		public ParamNotExistException() {
			super();
		}

		public ParamNotExistException(String message, Throwable cause,
				boolean enableSuppression, boolean writableStackTrace) {
			super(message, cause, enableSuppression, writableStackTrace);
		}

		public ParamNotExistException(String message, Throwable cause) {
			super(message, cause);
		}

		public ParamNotExistException(String message) {
			super(message);
		}

		public ParamNotExistException(Throwable cause) {
			super(cause);
		}
	}
	
	private static SqlHelperInstance instance = new SqlHelperInstance(new JdbcUtils());
	public static SqlHelperInstance getInstance() {
		return instance;
	}
	public static void setInstance(SqlHelperInstance instance) {
		SqlHelper.instance = instance;
	}
	public static void setJdbcUtils(JdbcUtils jdbcUtils) {
		SqlHelper.instance = new SqlHelperInstance(jdbcUtils);
	}

	/**
	 * 查询多个
	 * @param sql
	 * @param objects
	 * @return
	 */
	public static List<Model> query(String sql, Object...objects){
		return instance.query(sql, objects);
	}
	
	
	/**
	 * 分页
	 * @param currentpage 当前页
	 * @param pagesize 每页显示数
	 * @param sql 
	 * @return
	 */
	public static PageView page(int currentpage, int pagesize, String sql, Object...objects){
		return instance.page(currentpage, pagesize, sql, objects);
	}
	
	/**
	 * 分页
	 * @param sql 
	 * @return
	 */
	public static PageView page(String sql, Object...objects){
		return instance.page(sql, objects);
	}
	
	/**
	 * 查询一个
	 * @param sql
	 * @param objects
	 * @return
	 */
	public static Model queryOne(String sql, Object...objects){
		return instance.queryOne(sql, objects);
	}
	
	/**
	 * 执行Sql，返回影响的条数
	 * @param sql
	 * @param objects
	 * @return
	 */
	public static int execute(String sql, Object...objects){
		return instance.execute(sql, objects);
	}
	
	/**
	 * 执行Sql，返回自动生成的列，比如主健（GENERATED_KEY），时间
	 * @param sql
	 * @param objects
	 * @return
	 */
	public static Long executeGeneratedKey(String sql, Object...objects){
		return instance.executeGeneratedKey(sql, objects);
	}
	
	public static List<String> alltables(){
		return instance.alltables();
	}
	
	
	public static void main(String[] args) {
		WebHelper.init(null, null);
		
		List<Model> a = query("select * from student where id not in(${~uuid:yyyy})");
		System.out.println(a);
		
		Url2SqlContext c = WebHelper.getContext();
		Map<String, String[]> m = new HashMap<String, String[]>();
		c.setParams(m);
		m.put("a.id", new String[]{"2"});
		m.put("a.name", new String[]{"gaollg"});
		List<Model> result = query("select * from student where id=${a.id} and name like ?", "gaollg");
		System.out.println(result);
		
		m.clear();
		m.put("ids", new String[]{"1", "2", "3"});
		result = query("select * from student where id in(${ids}) or id in(${~uuid})");
		System.out.println(result);
		
		System.out.println("-----------------");
		result = query("select * from student where id in(?) or id in(${~uuid})", new String[]{"1", "2", "3"}, 0);
		System.out.println(result);
		
		m.clear();
		m.put("name", new String[]{"Java"});
		m.put("age", new String[]{"44"});
//		long id = executeGeneratedKey("INSERT INTO `student` (`name`, `age`) VALUES ('Huhu', '0')", "");
//		System.out.println(id);
//		int count = execute("INSERT INTO `student` (`name`, `age`) VALUES ('Huhu', '0')", map);
//		System.out.println("count:" + count);
		WebHelper.remove();
	}
	
	public static void main2(String[] args) {
		String a = "a?b?c?d?e?f?ghijklmn?";
		StringBuffer sb = new StringBuffer();
		String key = "?KEY&=";//把这些Url关键字全占了, 不信还真有这样的参数名称
		char[] array = a.toCharArray();
		for(int i=0; i<array.length; i++){
			char c = array[i];
			if(c == '?'){
				sb.append(key + i);
			}else {
				sb.append(c);
			}
		}
		System.out.println(sb);
	}
	
}
