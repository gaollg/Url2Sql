package net.vsame.url2sql.url.impl;

import java.lang.reflect.Method;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import net.vsame.url2sql.helper.CacheHelper;


public class UrlConfig {
	
	private String url;//路径
	private Method method;//如果是Java, 需指定方法
	private String js;//如果是js, js不能为空
	private Map<String, String> vars = new LinkedHashMap<String, String>();//放置需集中管理的文本，如Sql
	private String v;//校验
	private CacheHelper.EnumCacheType cacheType = CacheHelper.EnumCacheType.NONE;//缓存类型
	private Long cacheTime;//缓存时间
	private List<String> cleanPathCaches;//清除缓存
	private String status;//状态, NULL, 表示可用false表示禁用, 其它信息为接口禁用状态描述
	private String desc;
	
	/**
	 * 将文本转换为对应Method
	 * @param method
	 * @return
	 */
	public static Method string2Method(String method){
		int index = method.lastIndexOf(".");
		String className = method.substring(0, index);
		String methodName = method.substring(index+1);
		try {
			Class<?> clazz = Class.forName(className);
			return clazz.getMethod(methodName);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		if(!url.startsWith("/")){
			url = "/" + url;
		}
		this.url = url;
	}
	public Method getMethod() {
		return method;
	}
	public void setMethod(Method method) {
		this.method = method;
	}
	public void setMethod(String method) {
		this.method = string2Method(method);
	}
	public String getJs() {
		return js;
	}
	public void setJs(String js) {
		this.js = js;
	}
	public Map<String, String> getVars() {
		return vars;
	}
	public String getVar(String key) {
		String retVal = vars.get(key);
		if(retVal == null){
			throw new RuntimeException(key + " is null");
		}
		return retVal;
	}
	public void setVars(Map<String, String> vars) {
		this.vars = vars;
	}
	public String getV() {
		return v;
	}
	public void setV(String v) {
		this.v = v;
	}
	public CacheHelper.EnumCacheType getCacheType() {
		return cacheType;
	}
	public void setCacheType(CacheHelper.EnumCacheType cacheType) {
		if(cacheType == null) {
			cacheType = CacheHelper.EnumCacheType.NONE;
		}
		this.cacheType = cacheType;
	}
	public Long getCacheTime() {
		return cacheTime;
	}
	public void setCacheTime(Long cacheTime) {
		this.cacheTime = cacheTime;
	}
	public List<String> getCleanPathCaches() {
		return cleanPathCaches;
	}
	public void setCleanPathCaches(List<String> cleanPathCaches) {
		this.cleanPathCaches = cleanPathCaches;
	}
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = null;
		if(status == null){
			return;
		}
		status = status.trim();
		if("".equals(status)){
			return;
		}
		if("false".equals(status)){
			this.status = "disabled!";
		}else {
			this.status = status;
		}
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}

	/**
	 * 测试 setMethod方法
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		Method method = UrlConfig.class.getMethod("getV");
		System.out.println(method.getDeclaringClass());
		UrlConfig u = new UrlConfig();
		u.setMethod("net.vsame.url2sql.url.UrlConfig.getV");
		System.out.println(u.getMethod());
	}

	@Override
	public String toString() {
		return "UrlConfig [url=" + url + ", method=" + method + ", js=" + js
				+ ", vars=" + vars + ", v=" + v + ", cacheScript="
				+ cacheType + ", cleanCache=" + cleanPathCaches + ", status="
				+ status + ", desc=" + desc + "]";
	}
	
}
