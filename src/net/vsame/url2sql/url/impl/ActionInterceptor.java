package net.vsame.url2sql.url.impl;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.servlet.ServletContext;

import net.vsame.url2sql.helper.SqlHelper;
import net.vsame.url2sql.helper.Url2SqlContext;
import net.vsame.url2sql.helper.WebHelper;
import net.vsame.url2sql.url.Interceptor;
import net.vsame.url2sql.url.impl.UrlMapping.Chain;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;

/**
 * Action 执行拦截器
 * @author <a href="mailto:gaollg@sina.com">Gaollg</a>
 *
 */
public class ActionInterceptor implements Interceptor{
	
	private static String PARAM_NOT_EXIST_EXCEPTION = "net.vsame.url2sql.helper.SqlHelper$ParamNotExistException: ";
	
	private static Invocable jsFunction = null;
	
	//================================ 加载js ==========================
	static {
		resetJsInvocable();
	}
	
	private static void loadResource(ScriptEngine engine, String resource){
		// Run InvScript.js
		Reader scriptReader = new InputStreamReader(ActionInterceptor.class.getResourceAsStream(resource), Charset.forName("utf-8"));
		try {
			engine.eval(scriptReader);
		}catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			try {
				scriptReader.close();
			} catch (IOException e) {
			}
		}
	}
	
	public static void resetJsInvocable() {
		ScriptEngineManager manager = new ScriptEngineManager();
		ScriptEngine engine = manager.getEngineByName("JavaScript");
		loadResource(engine, "/core.js");
		if (engine instanceof Invocable) {
			jsFunction = (Invocable) engine;
		}else{
			throw new RuntimeException("not suppont!");
		}
	}
	
	@Override
	public void init(UrlMapping urlMapping, ServletContext servletContext) {
	}
	
	@Override
	public void invoke(Chain chain) {
		//调用Action
		Url2SqlContext c = WebHelper.getContext();
		UrlConfig u = c.getUrlConfig();
		Method m = u.getMethod();
		if(m != null){
			try {
				m.invoke(c.getAction());
			} catch (InvocationTargetException e) {
				Throwable t = e.getTargetException();
				if(t instanceof RuntimeException) {
					throw (RuntimeException) t;
				}
				throw new RuntimeException(e.getCause());
			} catch (Exception e) {
				throw new RuntimeException(e.getCause());
			}
		}else {
			executeJs();
		}
	}
	
	
	@Override
	public void destroy() {
		
	}
	
	public void executeJs(){
		List<String> errors = new ArrayList<String>();
		try {
			Url2SqlContext c = WebHelper.getContext();
			UrlConfig urlConfig = c.getUrlConfig();
			String json = "var vars="+ JSON.toJSONString(urlConfig.getVars(), SerializerFeature.WriteDateUseDateFormat)+";";
			jsFunction.invokeFunction("execute", json + urlConfig.getJs(), c, errors);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		if(!errors.isEmpty()){
			String msg = errors.get(0);
			if(msg!=null && msg.startsWith(PARAM_NOT_EXIST_EXCEPTION)){
				msg = msg.substring(PARAM_NOT_EXIST_EXCEPTION.length(), msg.length()-1);
				throw new SqlHelper.ParamNotExistException(msg);
			}
			throw new RuntimeException(msg);
		}
	}
	
	public static void main(String[] args) {
		Url2SqlContext c = WebHelper.getContext();
		UrlConfig urlConfig = new UrlConfig();
		c.setUrlConfig(urlConfig);
		c.setParams(new HashMap<String, String[]>());
		urlConfig.setJs("println('=========================');" +
				"context.put('a', 'b');" +
				"context.put('page', SQL.page('select *', 'from student', '1'));" +
				"context.put('page', SQL.alltables());" +
				"context.put('queryAllResult', SqlHelper.query('select * from student where id in(?)', [4,2,3]));");
		
		new ActionInterceptor().executeJs();
		System.out.println(c.getDatas());
		SqlHelper.class.getMethods();
	}
	
}
