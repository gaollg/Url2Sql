package net.vsame.url2sql.url.impl;

import java.util.Map;

import javax.servlet.ServletContext;

import net.vsame.url2sql.action.validator.ValidatorHelper;
import net.vsame.url2sql.helper.Url2SqlContext;
import net.vsame.url2sql.helper.WebHelper;
import net.vsame.url2sql.url.Interceptor;
import net.vsame.url2sql.url.impl.UrlMapping.Chain;

/**
 * 拦截器配置
 * @author <a href="mailto:gaollg@sina.com">Gaollg</a>
 *
 */
public class ValidatorInterceptor implements Interceptor{

	@Override
	public void init(UrlMapping urlMapping, ServletContext servletContext) {
	}
	@Override
	public void invoke(Chain chain) {
		Url2SqlContext c = WebHelper.getContext();
		Map<String, String[]> map = c.getParams();
		String v = c.getUrlConfig().getV();
		if(v == null){
			chain.next();
			return;
		}
		Map<String, String> errors = ValidatorHelper.v(map, v);
		if(errors.isEmpty()){
			chain.next();
		}else {
			c.putFormError("Params Error!", errors);
		}
	}
	@Override
	public void destroy() {
		
	}
	
}
