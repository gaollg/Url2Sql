0.0.0.5
	1, Context中请求路径为不包含扩展名路径
	2, 避免fastjson转json包含$ref 这样的对象引用
	3, 增加ant打包，忽略测试文件
	4, 修正加载拦截器BUG

0.0.0.6
	1, Model中增加getString(), 增加getXtype("name", default);
	2, 增加新的分页实现p()，原分页page()标记为过时
	3，增加类似JSP的PageScope的概念（TODO）
	4，移除restful风格（TODO）

0.0.0.7
	1, 支持maven
	2, 调整目录结构
	
0.0.0.8
	1, JSR223兼容JDK8
	2, 修改校验正则
	3, 公开获取Url方法
	
0.0.0.9
	1, 移除Session （完成）
	2, 增加context.putParam();（完成）
	3, ${param}不再抛出异常，校验拦截器就增加了（完成）
	
0.0.0.10
	1, 	
	
	
	